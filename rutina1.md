##Dias: Lunes - Miercoles - Viernes - Domingo

#Semana 1-2

##Lunes: 

##Fuerza: 
-Peso muerto: 3 series de 12 reps 60% de un RM
-Sentadilla con barra: 3 series de 12 reps 60% de un RM
-Prensa inclinada: 3 series de 12 reps 60% de un RM
-Aductores de cadera: 3 series de 12 reps 60% de un RM
-Patada hacia atrás con polea: 3 series de 12 reps 60% de un RM
-Sentadilla hack invertida: 3 series de 12 reps 60% de un RM
-Paseo muerto bulgaro: 3 series de 12 reps 60% de un RM
-Levantamientos olímpicos: 3 series de 6 reps 60% de un RM

##Pliometría:
-Kettlebell swing: 3 series a 1 min 
-Saltos de caja: 3 series a 1 minuto
-Sentadillas con salto: 3 series a 1 minuto
-Saltar estocadas: 3 series a 1 minuto

##Explosividad:
-Balanceo con kettlebells: 3 de 1 min 
-pull-ups: 3 series de 8
-Push press con kettlebells: 3 series de 8  cada mano

##Movilidad 
-Caminata de pato: 2 vueltas - https://www.youtube.com/watch?v=14BjRxE7f1o
-Sneaking ape: 2 vueltas  
-Bear-crab roll: 4 min
-Puentes 40
-Camarones 40
-Sit-outs 40
-Hits-ups: 20


#Miércoles: 

##Fuerza: 
-Press cbum: 3 series de 12 reps 60% de un RM
-Press plana: 3 series de 12 reps 60% de un RM
-Press banca con mancuernas: 3 series de 12 reps 60% de un RM
-Press inclinado con maquina: 3 series de 12 reps 60% de un RM
-Aperturas con maquina: 3 series de 12 reps 60% de un RM
-Empujones con maquina: 3 series de 12 reps 60% de un RM
-Ejercicio agarre con barra: 3 series de 12 reps 60% de un RM
-Curls con mancuerna alternado: 3 series de 12 reps 60% de un RM
-Curl Inclinado con mancuerna: 3 series de 12 reps 60% de un RM
-Curl martillo: 3 series de 12 reps 60% de un RM
-Curl Zorrman: 3 series de 12 reps 60% de un RM
-Remo en pecho: 3 series de 12 reps 60% de un RM

##Pliometría:
-Kettlebell swing: 3 series a 1 min 
-Saltos de caja: 3 series a 1 minuto
-Sentadillas con salto: 3 series a 1 minuto
-Saltar estocadas: 3 series a 1 minuto
-Push ups: 3 Series de 1 minutos

##Explosividad:
-Balanceo con kettlebells: 3 de 1 min 
-pull-ups: 3 series de 8
-Push press con kettlebells: 3 series de 8  cada mano

##Movilidad 
-Caminata de pato: 2 vueltas - https://www.youtube.com/watch?v=14BjRxE7f1o
-Sneaking ape: 2 vueltas  
-Bear-crab roll: 4 min
-Puentes 40
-Camarones 40
-Sit-outs 40
-Hits-ups: 20
